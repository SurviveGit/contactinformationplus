﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleWebApp.Controllers {
	public class HomeController : Controller {
		public ActionResult Index() {
			return View();
		}

		public ActionResult About() {
			ViewBag.Message = "Demonstration application for Git Survival Guide";

			return View();
		}

		public ActionResult Contact() {
			ViewBag.Message = "Granlund Solutions Oy";

			return View();
		}
	}
}